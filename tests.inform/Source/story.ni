"Tests" by Nathanaël Marion (in French)

[Ce projet permet d'effectuer les tests de l'extension. Nécessite Unit Testing de Nathanael Marion (disponible à https://gitlab.com/Natrium729/extensions-inform-7).

Les tests sont écrits dans les extensions propres au projet, dans le dossier « tests.materials/Extensions/Tests ».]

Use MAX_NUM_STATIC_STRINGS of 40000.

[Comme on ne peut pas modifier les options en cours de partie, il faut les activer manuellement quand on veut les tester. Idéalement, on ferait un projet de test par combinaison d'options, mais ça en ferait beaucoup.]
[Use variante soixante-dix.]
[Use variante septante.]
[Use variante quatre-vingts.]
[Use variante huitante.]
[Use variante octante.]
[Use variante quatre-vingt-dix.]
[Use variante nonante.]
[Use Belgian dialect.]
[Use Swiss dialect.]

Include Unit Testing by Nathanael Marion.

When play begins:
	try running all unit tests;

[L'endroit obligatoire.]
La Salle de test est un endroit.

[Un autre endroit utile pour les tests.]
Le Septentrion est au nord de la salle de test.

Volume - Phrases utilitaires

Book - Remettre le monde à zéro

To reset the world:
	move yourself to la Salle de test, without printing a room description;
	now le player est yourself;
	now le player est singular-named;
	now everything which is not the player est nowhere;
	now le story viewpoint est la second person plural;
	now le story tense est le present tense.

Volume - Des objets que les tests pourront utiliser

Book - Des choses sans particularités

Le machin est une chose.
La babiole est une chose.

Book - Une chose masculin singulier

Le concept-MS est une chose.

Book - Une chose masculin singulier dont le nom commence par une voyelle

L' aconcept-MS est une chose.

Book - Une chose masculin pluriel

Les concepts-MP sont une chose.

Book - Une chose masculin pluriel dont le nom commence par une voyelle

Les aconcepts-MP sont une chose.

Book - Une chose féminin singulier

La notion-FS est une chose.

Book - Une chose féminin singulier dont le nom commence par une voyelle

L' anotion-FS (f) est une chose.

Book - Une chose féminin pluriel

Les notions-FP (f) sont une chose.

Book - Une chose féminin pluriel dont le nom commence par une voyelle

Les anotions-FP (f) sont une chose.

Book -  Un homme

Gustave est un homme.

Book - Un homme dont le nom commence par une voyelle

Albert est un homme.

Book - Une femme

Céleste est une femme.
Understand "celeste" as Céleste.

Book - Une femme dont le nom commence par une voyelle

Aoda est une femme.

Book - Une chose dont le nom commence par un H aspiré

Le haricot est une chose.

Include (-
	with articles "Le " "le " "un ",
-) when defining le haricot.

Book - Une chose dont le nom commence par un oe collé

l' oeuf est une chose. Le printed name est "œuf".

Include (-
	with articles "L'" "l'" "un ",
-) when defining l' oeuf.

Book - Une chose dont l'indefinite article est un article défini

l' Europe (f) est une chose.

Include (-
	with articles "L'" "l'" "l'",
-) when defining l' Europe.

Book - Un contenant

La cage est un contenant.

Book - Un petit contenant entrable

La boîte est un contenant enterable.
Le carrying capacity est 1.

Book - Un support

La table est un support.

Book - Un petit support entrable

Le tabouret est un enterable support.
Le carrying capacity est 1.

Book - Une chose décorative

Le décor est une chose scenery.

Book - Une chose mettable

La veste est une chose wearable.

Book - Un fourre-tout

Le sac est un fourre-tout.

Volume - L'inclusion des tests

Include Determinants by Tests.
Include Directions by Tests.
Include Nombres by Tests.
Include Pronoms by Tests.
Include Heures by Tests.
Include Reponses by Tests.
