Version 1/170621 of French Imperative Commands by Nathanael Marion begins here.

"Allows the player to enter commands at the second person plural or singular imperative in French. Requires French Language by Nathanael Marion.".

Use authorial modesty.
Use MAX_VERBSPACE of 8192.

Section 1 - A

Understand the command "abandonne" and "abandonnez" as "abandonner".
Understand the command "achete" and "achetez" as "acheter".
Understand the command "agite" and "agitez" as "agiter".
Understand the command "ajuste" and "ajustez" as "ajuster".
Understand the command "va" and "allez" as "aller".
Understand the command "allonge" and "allongez" as "allonger".
Understand the command "allume" and "allumez" as "allumer".
Understand the command "appuie" and "appuyez" as "appuyer".
Understand the command "arrete" and "arretez" as "arreter".
Understand the command "assois", "assieds", "assoyez" and "asseyez" as "asseoir".
Understand the command "astique" and "astiquez" as "astiquer".
Understand the command "attache" and "attachez" as "attacher".
Understand the command "attaque" and "attaquez" as "attaquer".
Understand the command "attends" and "attendez" as "attendre".
Understand the command "avale" and "avalez" as "avaler".

Section 2 - B

Understand the command "balance" and "balancez" as "balancer".
Understand the command "balaye", "balaie" and "balayez" as "balayer".
Understand the command "bondis" and "bondissez" as "bondir".
Understand the command "bois" and "buvez" as "boire".
Understand the command "bouge" and "bougez" as "bouger".
Understand the command "branche" and "branchez" as "brancher".
Understand the command "brandis" and "brandissez" as "brandir".
Understand the command "brise" and "brisez" as "briser".
Understand the command "brule" and "brulez" as "bruler".

Section 3 - C

Understand the command "caresse" and "caressez" as "caresser".
Understand the command "casse" and "cassez" as "casser".
Understand the command "cherche" and "cherchez" as "chercher".
Understand the command "cire" and "cirez" as "cirer".
Understand the command "cogne" and "cognez" as "cogner".
Understand the command "combats" and "combattez" as "combattre".
Understand the command "commute" and "commutez" as "commuter".
Understand the command "comprime" and "comprimez" as "comprimer".
Understand the command "connecte" and "connectez" as "connecter".
Understand the command "consulte" and "consultez" as "consulter".
Understand the command "couche" and "couchez" as "coucher". 
Understand the command "coupe" and "coupez" as "couper".
Understand the command "cours" and "courez" as "courir".
Understand the command "crame" and "cramez" as "cramer".
Understand the command "crie" and "criez" as "crier".
Understand the command "cueille" and "cueillez" as "cueillir".

Section 4 - D

Understand the command "decolle" and "decollez" as "decoller".
Understand the command "decoupe" and "decoupez" as "decouper".
Understand the command "decris" and "decrivez" as "decrire".
Understand the command "demande" and "demandez" as "demander".
Understand the command "demarre" and "demarrez" as "demarrer".
Understand the command "deplace" and "deplacez" as "deplacer".
Understand the command "depose" and "deposez" as "deposer".
Understand the command "depoussiere" and "depoussierez" as "depoussierer".
Understand the command "descends" and "descendez" as "descendre".
Understand the command "detruis" and "detruisez" as "detruire".
Understand the command "deverrouille" and "deverrouillez" as "deverrouiller".
Understand the command "devisse" and "devissez" as "devisser".
Understand the command "devore" and "devorez" as "devorer".
Understand the command "dis" and "dites" as "dire".
Understand the command "donne" and "donnez" as "donner".
Understand the command "dors" and "dormez" as "dormir".

Section 5 - E

Understand the command "ecoute" and "ecoutez" as "ecouter".
Understand the command "ecrase" and "ecrasez" as "ecraser".
Understand the command "elague" and "elaguez" as "elaguer".
Understand the command "embrase" and "embrasez" as "embraser".
Understand the command "embrasse" and "embrassez" as "embrasser".
Understand the command "emprunte" and "empruntez" as "emprunter".
Understand the command "enfile" and "enfilez" as "enfiler".
Understand the command "enleve" and "enlevez" as "enlever".
Understand the command "entends" and "entendez" as "entendre".
Understand the command "entre" and "entrez" as "entrer".
Understand the command "escalade" and "escaladez" as "escalader".
Understand the command "essuie" and "essuyez" as "essuyer".
Understand the command "eteins" and "eteignez" as "eteindre".
Understand the command "etreins" and "etreignez" as "etreindre".
Understand the command "eveille" and "eveillez" as "eveiller".
Understand the command ["examine" and] "examinez" as "examiner".
Understand the command "excuse" and "excusez" as "excuser".

Section 6 - F

Understand the command "farfouille" and "farfouillez" as "farfouiller".
Understand the command "ferme" and "fermez" as "fermer".
Understand the command "fixe" and "fixez" as "fixer".
Understand the command "force" and "forcez" as "forcer".
Understand the command "fouille" and "fouillez" as "fouiller".
Understand the command "franchis" and "franchissez" as "franchir".
Understand the command "frappe" and "frappez" as "frapper".
Understand the command "frotte" and "frottez" as "frotter".
Understand the command "fuis" and "fuyez" as "fuir".

Section 7 - G

Understand the command "goute" and "goutez" as "gouter".
Understand the command "gravis" and "gravissez" as "gravir".
Understand the command "grignote" and "grignotez" as "grignoter".
Understand the command "grimpe" and "grimpez" as "grimper".

Section 8 - H

Understand the command "habille" and "habillez" as "habiller".
Understand the command "hume" and "humez" as "humer".
Understand the command "hurle" and "hurlez" as "hurler".

Section 9 - I

Understand the command "incendie" and "incendiez" as "incendier".
Understand the command "ingere" and "ingerez" as "ingerer".
Understand the command "insere" and "inserez" as "inserer".

Section 10 - J

Understand the command "jette" and "jetez" as "jeter".

Section 11 - K

Section 12 - L

Understand the command "lache" and "lachez" as "lacher".
Understand the command "laisse" and "laissez" as "laisser".
Understand the command "lance" and "lancez" as "lancer".
Understand the command "leve" and "levez" as "lever".
Understand the command "lis" and "lisez" as "lire".

Section 13 - M

Understand the command "mange" and "mangez" as "manger".
Understand the command "marche" and "marchez" as "marcher".
Understand the command "mastique" and "mastiquez" as "mastiquer".
Understand the command "mets" and "mettez" as "mettre".
Understand the command "monte" and "montez" as "monter".
Understand the command "montre" and "montrez" as "montrer".

Section 14 - N

Understand the command "nettoie" and "nettoyez" as "nettoyer".
Understand the command "noue" and "nouez" as "nouer".
Understand the command "nourris" and "nourrissez" as "nourrir".

Section 15 - O

Understand the command "observe" and "observez" as "observer".
Understand the command "offre" and "offrez" as "offrir".
Understand the command "oscille" and "oscillez" as "osciller".
Understand the command "ouvre" and "ouvrez" as "ouvrir".

Section 16 - P

Understand the command "palpe" and "palpez" as "palper".
Understand the command "pars" and "partez" as "partir".
Understand the command "passe" and "passez" as "passer".
Understand the command "patiente" and "patientez" as "patienter".
Understand the command "paye", "paie", and "payez" as "payer".
Understand the command "pends" and "pendez" as "pendre".
Understand the command "pense" and "pensez" as "penser".
Understand the command "pointe" and "pointez" as "pointer".
Understand the command "pose" and "posez" as "poser".
Understand the command "pousse" and "poussez" as "pousser".
Understand the command "prends" and "prenez" as "prendre".
Understand the command "presente" and "presentez" as "presenter".
Understand the command "presse" and "pressez" as "presser".

Section 17 - Q

Understand the command "questionne" and "questionnez" as "questionner".

Section 18 - R

Understand the command "raconte" and "racontez" as "raconter".
Understand the command "ramasse" and "ramassez" as "ramasser".
Understand the command "redescends" and "redescendez" as "redescendre".
Understand the command "recure" and "recurez" as "recurer".
Understand the command "reflechis" and "reflechissez" as "reflechir".
Understand the command "regarde" and "regardez" as "regarder".
Understand the command "regle" and "reglez" as "regler".
Understand the command "releve" and "relevez" as "relever".
Understand the command "remets" and "remettez" as "remettre".
Understand the command "remonte" and "remontez" as "remonter".
Understand the command "renifle" and "reniflez" as "renifler".
Understand the command "rentre" and "rentrez" as "rentrer".
Understand the command "reponds" and "repondez" as "repondre".
Understand the command "retire" and "retirez" as "retirer".
Understand the command "reveille" and "reveillez" as "reveiller".
Understand the command "revets" and "revetez" as "revetir".

Section 19 - S

Understand the command "salue" and "saluez" as "saluer".
Understand the command "saute" and "sautez" as "sauter".
Understand the command "secoue" and "secouez" as "secouer".
Understand the command "sens" and "sentez" as "sentir".
Understand the command "sirote" and "sirotez" as "siroter".
Understand the command "somnole" and "somnolez" as "somnoler".
Understand the command "sors" and "sortez" as "sortir".
Understand the command "souleve" and "soulevez" as "soulever".
Understand the command "suis" and "suivez" as "suivre".
Understand the command "suspends" and "suspendez" as "suspendre".

Section 20 - T

Understand the command "tate" and "tatez" as "tater".
Understand the command "tire" and "tirez" as "tirer".
Understand the command "tords" and "tordez" as "tordre".
Understand the command ["torture" and] "torturez" as "torturer".
Understand the command "touche" and "touchez" as "toucher".
Understand the command "tourne" and "tournez" as "tourner".
Understand the command "traine" and "trainez" as "trainer".
Understand the command "tranche" and "tranchez" as "trancher".
Understand the command "tue" and "tuez" as "tuer".

Section 21 - U

Section 22 - V

Understand the command "verrouille" and "verrouillez" as "verrouiller".
Understand the command "visse" and "vissez" as "visser".
Understand the command "vois" and "voyez" as "voir".

Section 23 - W

Section 24 - X

Section 25 - Y

Section 26 - Z

French Imperative Commands ends here.

---- DOCUMENTATION ----

Cette extension permet au joueur de taper des commandes à la deuxième personne du singulier ou du pluriel de l'impératif :

	> prends la clef
	> va au nord
	> ouvrez la porte
	> mettez les lunettes sur la table
	etc.

Ceci est particulièrement utile pour les jeux où le joueur doit donner des ordres aux autres personnages :

	> Bob, allume l'ordinateur
	> Alice, buvez le thé
