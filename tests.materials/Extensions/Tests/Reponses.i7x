Version 1/210414 of Reponses by Tests begins here.

Book - Les response tests

A response test is a kind of unit test.

Before testing a response test:
	reset the world;

After testing a response test:
	reset the world;

Book - Phrases utiles pour tester les réponses

[Ces deux phrases exécutent la règle donnée en argument pour chaque combinaison personne/temps, et comparent le texte produit à la liste donnée en argument. Les éléments de la liste doivent se conformer au tableau plus bas.

On pourrait utiliser un tableau pour chaque test plutôt qu'une liste, et ce serait je crois plus rapide, mais il faudrait alors déclarer tout un tableau de textes attendus pour chaque réponse et ça serait lourd à écrire. Lors de mes tests, les deux méthodes prenaient un temps très similaire.]

To test (rsp - a response) with (rl - a rule) against (L - a list of texts):
	assert "testing [rsp] with all tense/viewpoint combinations" that number of entries of L equals 24;
	repeat with row running from 1 to the number of rows in table of viewpoints-tenses:
		choose row row in table of viewpoints-tenses;
		now le story viewpoint est point de vue entry;
		if story viewpoint est third person plural:
			now le player est plural-named;
		else:
			now le player est singular-named;
		now le story tense est temps entry;
		follow rl;
		assert "[rsp], [story viewpoint], [story tense]" that "[captured output]" exactly matches the text "[entry row of L]";

To test (rsp - a response) with the/-- line (N - a number) of/in (rl - a rule) against (L - a list of texts):
	assert "testing [rsp] with all tense/viewpoint combinations" that number of entries of L equals 24;
	repeat with row running from 1 to the number of rows in table of viewpoints-tenses:
		choose row row in table of viewpoints-tenses;
		now le story viewpoint est point de vue entry;
		if story viewpoint est third person plural:
			now le player est plural-named;
		else:
			now le player est singular-named;
		now le story tense est temps entry;
		follow rl;
		let received be line number N in captured output;
		assert "[rsp], [story viewpoint], [story tense]" that "[received]" exactly matches the text "[entry row of L]";

Table of viewpoints-tenses
point de vue	temps
first person singular	present tense
first person singular	past historic tense
first person singular	perfect tense
first person singular	future tense
second person singular	present tense
second person singular	past historic tense
second person singular	perfect tense
second person singular	future tense
third person singular	present tense
third person singular	past historic tense
third person singular	perfect tense
third person singular	future tense
first person plural	present tense
first person plural	past historic tense
first person plural	perfect tense
first person plural	future tense
second person plural	present tense
second person plural	past historic tense
second person plural	perfect tense
second person plural	future tense
third person plural	present tense
third person plural	past historic tense
third person plural	perfect tense
third person plural	future tense

Book - The final question

Simple unit test:
	let R be the print the final prompt rule response (A);
	assert "[R]" that "[text of R]" exactly matches the text ">[run paragraph on]";

Simple unit test:
	let R be the print the final question rule response (A);
	assert "[R]" that "[text of R]" exactly matches the text "Voulez-vous ";
	let R be the print the final question rule response (B);
	assert "[R]" that "[text of R]" exactly matches the text " ou ";
	let R be the standard respond to final question rule response (A);
	assert "[R]" that "[text of R]" exactly matches the text "Faites un choix parmi les propositions ci-dessus.";

Book - Printing the locale description

[TODO]

Book - Standard actions concerning the actor's possessions

Part - Taking inventory

Chapter - Empty inventory

RT-empty-inventory-A is a response test.

For testing RT-empty-inventory-A:
	test the print empty inventory rule response (A) with the print empty inventory test rule against {
		"Je n'ai rien.",
		"Je n'avais rien.",
		"Je n'avais rien.",
		"Je n'aurai rien.",
		"Tu n'as rien.",
		"Tu n'avais rien.",
		"Tu n'avais rien.",
		"Tu n'auras rien.",
		"Il n'a rien.",
		"Il n'avait rien.",
		"Il n'avait rien.",
		"Il n'aura rien.",
		"Nous n'avons rien.",
		"Nous n'avions rien.",
		"Nous n'avions rien.",
		"Nous n'aurons rien.",
		"Vous n'avez rien.",
		"Vous n'aviez rien.",
		"Vous n'aviez rien.",
		"Vous n'aurez rien.",
		"Ils n'ont rien.",
		"Ils n'avaient rien.",
		"Ils n'avaient rien.",
		"Ils n'auront rien."
	};

This is the print empty inventory test rule:
	try taking inventory.

Chapter - Standard inventory

RT-standard-inventory-A is a response test.

For testing RT-standard-inventory-A:
	now le player porte le machin;
	test the print standard inventory rule response (A) with the line 1 of the print standard inventory test rule against {
		"J'ai[_]:",
		"J'avais[_]:",
		"J'avais[_]:",
		"J'aurai[_]:",
		"Tu as[_]:",
		"Tu avais[_]:",
		"Tu avais[_]:",
		"Tu auras[_]:",
		"Il a[_]:",
		"Il avait[_]:",
		"Il avait[_]:",
		"Il aura[_]:",
		"Nous avons[_]:",
		"Nous avions[_]:",
		"Nous avions[_]:",
		"Nous aurons[_]:",
		"Vous avez[_]:",
		"Vous aviez[_]:",
		"Vous aviez[_]:",
		"Vous aurez[_]:",
		"Ils ont[_]:",
		"Ils avaient[_]:",
		"Ils avaient[_]:",
		"Ils auront[_]:"
	}.

This is the print standard inventory test rule:
	try taking inventory.

Part - Taking

Chapter - Taking yourself

RT-can't-take-yourself-A is a  response test.

For testing RT-can't-take-yourself-A::
	test the can't take yourself rule response (A) with the can't take yourself test rule against {
		"Je me possède moi-même. Voilà une problématique sur laquelle les philosophes n'ont pas dû passer beaucoup de temps.",
		"Je me possédais moi-même. Voilà une problématique sur laquelle les philosophes n'avaient pas dû passer beaucoup de temps.",
		"Je me possédais moi-même. Voilà une problématique sur laquelle les philosophes n'avaient pas dû passer beaucoup de temps.",
		"Je me posséderai moi-même. Voilà une problématique sur laquelle les philosophes n'auront pas dû passer beaucoup de temps.",
		"Tu te possèdes toi-même. Voilà une problématique sur laquelle les philosophes n'ont pas dû passer beaucoup de temps.",
		"Tu te possédais toi-même. Voilà une problématique sur laquelle les philosophes n'avaient pas dû passer beaucoup de temps.",
		"Tu te possédais toi-même. Voilà une problématique sur laquelle les philosophes n'avaient pas dû passer beaucoup de temps.",
		"Tu te posséderas toi-même. Voilà une problématique sur laquelle les philosophes n'auront pas dû passer beaucoup de temps.",
		"Il se possède lui-même. Voilà une problématique sur laquelle les philosophes n'ont pas dû passer beaucoup de temps.",
		"Il se possédait lui-même. Voilà une problématique sur laquelle les philosophes n'avaient pas dû passer beaucoup de temps.",
		"Il se possédait lui-même. Voilà une problématique sur laquelle les philosophes n'avaient pas dû passer beaucoup de temps.",
		"Il se possédera lui-même. Voilà une problématique sur laquelle les philosophes n'auront pas dû passer beaucoup de temps.",
		"Nous nous possédons nous-même. Voilà une problématique sur laquelle les philosophes n'ont pas dû passer beaucoup de temps.",
		"Nous nous possédions nous-même. Voilà une problématique sur laquelle les philosophes n'avaient pas dû passer beaucoup de temps.",
		"Nous nous possédions nous-même. Voilà une problématique sur laquelle les philosophes n'avaient pas dû passer beaucoup de temps.",
		"Nous nous posséderons nous-même. Voilà une problématique sur laquelle les philosophes n'auront pas dû passer beaucoup de temps.",
		"Vous vous possédez vous-même. Voilà une problématique sur laquelle les philosophes n'ont pas dû passer beaucoup de temps.",
		"Vous vous possédiez vous-même. Voilà une problématique sur laquelle les philosophes n'avaient pas dû passer beaucoup de temps.",
		"Vous vous possédiez vous-même. Voilà une problématique sur laquelle les philosophes n'avaient pas dû passer beaucoup de temps.",
		"Vous vous posséderez vous-même. Voilà une problématique sur laquelle les philosophes n'auront pas dû passer beaucoup de temps.",
		"Ils se possèdent eux-mêmes. Voilà une problématique sur laquelle les philosophes n'ont pas dû passer beaucoup de temps.",
		"Ils se possédaient eux-mêmes. Voilà une problématique sur laquelle les philosophes n'avaient pas dû passer beaucoup de temps.",
		"Ils se possédaient eux-mêmes. Voilà une problématique sur laquelle les philosophes n'avaient pas dû passer beaucoup de temps.",
		"Ils se posséderont eux-mêmes. Voilà une problématique sur laquelle les philosophes n'auront pas dû passer beaucoup de temps."
	};

This is the can't take yourself test rule:
	try taking the player.

Chapter - Taking other people

RT-can't-take-other-people-A is a response test.

For testing RT-can't-take-other-people-A:
	now Gustave est dans la location;
	test the can't take other people rule response (A) with the can't take other people masculine singular test rule against {
		"Il n'apprécierait sûrement pas cela.",
		"Il n'aurait sûrement pas apprécié cela.",
		"Il n'aurait sûrement pas apprécié cela.",
		"Il n'appréciera sûrement pas cela.",
		"Il n'apprécierait sûrement pas cela.",
		"Il n'aurait sûrement pas apprécié cela.",
		"Il n'aurait sûrement pas apprécié cela.",
		"Il n'appréciera sûrement pas cela.",
		"Il n'apprécierait sûrement pas cela.",
		"Il n'aurait sûrement pas apprécié cela.",
		"Il n'aurait sûrement pas apprécié cela.",
		"Il n'appréciera sûrement pas cela.",
		"Il n'apprécierait sûrement pas cela.",
		"Il n'aurait sûrement pas apprécié cela.",
		"Il n'aurait sûrement pas apprécié cela.",
		"Il n'appréciera sûrement pas cela.",
		"Il n'apprécierait sûrement pas cela.",
		"Il n'aurait sûrement pas apprécié cela.",
		"Il n'aurait sûrement pas apprécié cela.",
		"Il n'appréciera sûrement pas cela.",
		"Il n'apprécierait sûrement pas cela.",
		"Il n'aurait sûrement pas apprécié cela.",
		"Il n'aurait sûrement pas apprécié cela.",
		"Il n'appréciera sûrement pas cela."
	};
	now Céleste est dans la location;
	now Céleste est plural-named;
	test the can't take other people rule response (A) with the can't take other people feminine plural test rule against {
		"Elles n'apprécieraient sûrement pas cela.",
		"Elles n'auraient sûrement pas apprécié cela.",
		"Elles n'auraient sûrement pas apprécié cela.",
		"Elles n'apprécieront sûrement pas cela.",
		"Elles n'apprécieraient sûrement pas cela.",
		"Elles n'auraient sûrement pas apprécié cela.",
		"Elles n'auraient sûrement pas apprécié cela.",
		"Elles n'apprécieront sûrement pas cela.",
		"Elles n'apprécieraient sûrement pas cela.",
		"Elles n'auraient sûrement pas apprécié cela.",
		"Elles n'auraient sûrement pas apprécié cela.",
		"Elles n'apprécieront sûrement pas cela.",
		"Elles n'apprécieraient sûrement pas cela.",
		"Elles n'auraient sûrement pas apprécié cela.",
		"Elles n'auraient sûrement pas apprécié cela.",
		"Elles n'apprécieront sûrement pas cela.",
		"Elles n'apprécieraient sûrement pas cela.",
		"Elles n'auraient sûrement pas apprécié cela.",
		"Elles n'auraient sûrement pas apprécié cela.",
		"Elles n'apprécieront sûrement pas cela.",
		"Elles n'apprécieraient sûrement pas cela.",
		"Elles n'auraient sûrement pas apprécié cela.",
		"Elles n'auraient sûrement pas apprécié cela.",
		"Elles n'apprécieront sûrement pas cela."
	};
	now Céleste est singular-named;

This is the can't take other people masculine singular test rule:
	try taking Gustave.

This is the can't take other people feminine plural test rule:
	try taking Céleste.

Chapter - Taking component parts

RT-can't-take-component-parts-A is a response test.

For testing RT-can't-take-component-parts-A:
	now le machin est une partie de la babiole;
	now la babiole est dans la location;
	test the can't take component parts rule response (A) with the can't take component parts test rule against {
		"Cela semble faire partie de la babiole.",
		"Cela semblait faire partie de la babiole.",
		"Cela semblait faire partie de la babiole.",
		"Cela semblera faire partie de la babiole.",
		"Cela semble faire partie de la babiole.",
		"Cela semblait faire partie de la babiole.",
		"Cela semblait faire partie de la babiole.",
		"Cela semblera faire partie de la babiole.",
		"Cela semble faire partie de la babiole.",
		"Cela semblait faire partie de la babiole.",
		"Cela semblait faire partie de la babiole.",
		"Cela semblera faire partie de la babiole.",
		"Cela semble faire partie de la babiole.",
		"Cela semblait faire partie de la babiole.",
		"Cela semblait faire partie de la babiole.",
		"Cela semblera faire partie de la babiole.",
		"Cela semble faire partie de la babiole.",
		"Cela semblait faire partie de la babiole.",
		"Cela semblait faire partie de la babiole.",
		"Cela semblera faire partie de la babiole.",
		"Cela semble faire partie de la babiole.",
		"Cela semblait faire partie de la babiole.",
		"Cela semblait faire partie de la babiole.",
		"Cela semblera faire partie de la babiole."
	};

This is the can't take component parts test rule:
	try taking le machin.

Chapter - Can't take people's possessions

RT-can't-take-people's-possessions-A is a response test.

For testing RT-can't-take-people's-possessions-A:
	now Céleste porte le machin;
	now Céleste est dans la location;
	test the can't take people's possessions rule response (A) with the can't take people's possessions test rule against {
		"Cela semble appartenir à Céleste.",
		"Cela semblait appartenir à Céleste.",
		"Cela semblait appartenir à Céleste.",
		"Cela semblera appartenir à Céleste.",
		"Cela semble appartenir à Céleste.",
		"Cela semblait appartenir à Céleste.",
		"Cela semblait appartenir à Céleste.",
		"Cela semblera appartenir à Céleste.",
		"Cela semble appartenir à Céleste.",
		"Cela semblait appartenir à Céleste.",
		"Cela semblait appartenir à Céleste.",
		"Cela semblera appartenir à Céleste.",
		"Cela semble appartenir à Céleste.",
		"Cela semblait appartenir à Céleste.",
		"Cela semblait appartenir à Céleste.",
		"Cela semblera appartenir à Céleste.",
		"Cela semble appartenir à Céleste.",
		"Cela semblait appartenir à Céleste.",
		"Cela semblait appartenir à Céleste.",
		"Cela semblera appartenir à Céleste.",
		"Cela semble appartenir à Céleste.",
		"Cela semblait appartenir à Céleste.",
		"Cela semblait appartenir à Céleste.",
		"Cela semblera appartenir à Céleste."
	};

This is the can't take people's possessions test rule:
	try taking le machin.

Chapter - Can't take items out of play

[Cette règle ne semble pas se lancer, car la basic accessibility rule la court-circuite toujours.]

[RT-can't take-items-out-of-play-A is a response test.

For testing RT-can't take-items-out-of-play-A:
	test the can't take items out of play rule response (A) with the can't take items out of play test rule against {
		"Les feuilles ne sont pas disponibles.",
		"Les feuilles n'étaient pas disponibles.",
		"Les feuilles n'étaient pas disponibles.",
		"Les feuilles ne seront pas disponibles.",
		"Les feuilles ne sont pas disponibles.",
		"Les feuilles n'étaient pas disponibles.",
		"Les feuilles n'étaient pas disponibles.",
		"Les feuilles ne seront pas disponibles.",
		"Les feuilles ne sont pas disponibles.",
		"Les feuilles n'étaient pas disponibles.",
		"Les feuilles n'étaient pas disponibles.",
		"Les feuilles ne seront pas disponibles.",
		"Les feuilles ne sont pas disponibles.",
		"Les feuilles n'étaient pas disponibles.",
		"Les feuilles n'étaient pas disponibles.",
		"Les feuilles ne seront pas disponibles.",
		present tense	"Les feuilles ne sont pas disponibles.",
		past historic tense	"Les feuilles n'étaient pas disponibles.",
		perfect tense	"Les feuilles n'étaient pas disponibles.",
		future tense	"Les feuilles ne seront pas disponibles.",
		"Les feuilles ne sont pas disponibles.",
		"Les feuilles n'étaient pas disponibles.",
		"Les feuilles n'étaient pas disponibles.",
		"Les feuilles ne seront pas disponibles."
	};

This is the can't take items out of play test rule:
	try taking les feuilles.]

Chapter - Can't take what you are inside (contenant)

RT-can't-take-what-you're-inside-A-container is a response test.

For testing RT-can't-take-what-you're-inside-A-container:
	now la cage est dans la location;
	move le player to la cage, without printing a room description;
	test the can't take what you're inside rule response (A) with the can't take what you're inside contenant test rule against {
		"Je dois d'abord sortir de la cage.",
		"Je devais d'abord sortir de la cage.",
		"Je devais d'abord sortir de la cage.",
		"Je devrai d'abord sortir de la cage.",
		"Tu dois d'abord sortir de la cage.",
		"Tu devais d'abord sortir de la cage.",
		"Tu devais d'abord sortir de la cage.",
		"Tu devras d'abord sortir de la cage.",
		"Il doit d'abord sortir de la cage.",
		"Il devait d'abord sortir de la cage.",
		"Il devait d'abord sortir de la cage.",
		"Il devra d'abord sortir de la cage.",
		"Nous devons d'abord sortir de la cage.",
		"Nous devions d'abord sortir de la cage.",
		"Nous devions d'abord sortir de la cage.",
		"Nous devrons d'abord sortir de la cage.",
		"Vous devez d'abord sortir de la cage.",
		"Vous deviez d'abord sortir de la cage.",
		"Vous deviez d'abord sortir de la cage.",
		"Vous devrez d'abord sortir de la cage.",
		"Ils doivent d'abord sortir de la cage.",
		"Ils devaient d'abord sortir de la cage.",
		"Ils devaient d'abord sortir de la cage.",
		"Ils devront d'abord sortir de la cage."
	};

This is the can't take what you're inside contenant test rule:
	try taking la cage.

Chapter - Can't take what you are inside (support)

RT-can't-take-what-you're-inside-A-supporter is a response test.

For testing RT-can't-take-what-you're-inside-A-supporter:
	now la table est dans la location;
	move le player to la table, without printing a room description;
	test the can't take what you're inside rule response (A) with the can't take what you're inside support test rule against {
		"Je dois d'abord descendre de la table.",
		"Je devais d'abord descendre de la table.",
		"Je devais d'abord descendre de la table.",
		"Je devrai d'abord descendre de la table.",
		"Tu dois d'abord descendre de la table.",
		"Tu devais d'abord descendre de la table.",
		"Tu devais d'abord descendre de la table.",
		"Tu devras d'abord descendre de la table.",
		"Il doit d'abord descendre de la table.",
		"Il devait d'abord descendre de la table.",
		"Il devait d'abord descendre de la table.",
		"Il devra d'abord descendre de la table.",
		"Nous devons d'abord descendre de la table.",
		"Nous devions d'abord descendre de la table.",
		"Nous devions d'abord descendre de la table.",
		"Nous devrons d'abord descendre de la table.",
		"Vous devez d'abord descendre de la table.",
		"Vous deviez d'abord descendre de la table.",
		"Vous deviez d'abord descendre de la table.",
		"Vous devrez d'abord descendre de la table.",
		"Ils doivent d'abord descendre de la table.",
		"Ils devaient d'abord descendre de la table.",
		"Ils devaient d'abord descendre de la table.",
		"Ils devront d'abord descendre de la table."
	};

This is the can't take what you're inside support test rule:
	try taking la table.

Chapter - Can't take what's already taken

RT-can't-take-what's-already-taken-A is a response test.

For testing RT-can't-take-what's-already-taken-A:
	now le player porte le machin;
	test the can't take what's already taken rule response (A) with the can't take what's already taken test rule against {
		"Je l'ai déjà.",
		"Je l'avais déjà.",
		"Je l'avais déjà.",
		"Je l'aurai déjà.",
		"Tu l'as déjà.",
		"Tu l'avais déjà.",
		"Tu l'avais déjà.",
		"Tu l'auras déjà.",
		"Il l'a déjà.",
		"Il l'avait déjà.",
		"Il l'avait déjà.",
		"Il l'aura déjà.",
		"Nous l'avons déjà.",
		"Nous l'avions déjà.",
		"Nous l'avions déjà.",
		"Nous l'aurons déjà.",
		"Vous l'avez déjà.",
		"Vous l'aviez déjà.",
		"Vous l'aviez déjà.",
		"Vous l'aurez déjà.",
		"Ils l'ont déjà.",
		"Ils l'avaient déjà.",
		"Ils l'avaient déjà.",
		"Ils l'auront déjà."
	};

This is the can't take what's already taken test rule:
	try taking le machin.

Chapter - Can't take scenery

RT-can't-take-scenery-A is a response test.

For testing RT-can't-take-scenery-A:
	now le décor est dans la location;
	test the can't take scenery rule response (A) with the can't take scenery test rule against {
		"C'est trop difficile à transporter.",
		"C'était trop difficile à transporter.",
		"C'était trop difficile à transporter.",
		"Ce sera trop difficile à transporter.",
		"C'est trop difficile à transporter.",
		"C'était trop difficile à transporter.",
		"C'était trop difficile à transporter.",
		"Ce sera trop difficile à transporter.",
		"C'est trop difficile à transporter.",
		"C'était trop difficile à transporter.",
		"C'était trop difficile à transporter.",
		"Ce sera trop difficile à transporter.",
		"C'est trop difficile à transporter.",
		"C'était trop difficile à transporter.",
		"C'était trop difficile à transporter.",
		"Ce sera trop difficile à transporter.",
		"C'est trop difficile à transporter.",
		"C'était trop difficile à transporter.",
		"C'était trop difficile à transporter.",
		"Ce sera trop difficile à transporter.",
		"C'est trop difficile à transporter.",
		"C'était trop difficile à transporter.",
		"C'était trop difficile à transporter.",
		"Ce sera trop difficile à transporter."
	};

This is the can't take scenery test rule:
	try taking le décor.

Chapter - Can only take things

RT-can-only-take-things-A is a response test.

For testing RT-can-only-take-things-A:
	test the can only take things rule response (A) with the can only take things test rule against {
		"Je ne peux pas transporter cela.",
		"Je ne pouvais pas transporter cela.",
		"Je ne pouvais pas transporter cela.",
		"Je ne pourrai pas transporter cela.",
		"Tu ne peux pas transporter cela.",
		"Tu ne pouvais pas transporter cela.",
		"Tu ne pouvais pas transporter cela.",
		"Tu ne pourras pas transporter cela.",
		"Il ne peut pas transporter cela.",
		"Il ne pouvait pas transporter cela.",
		"Il ne pouvait pas transporter cela.",
		"Il ne pourra pas transporter cela.",
		"Nous ne pouvons pas transporter cela.",
		"Nous ne pouvions pas transporter cela.",
		"Nous ne pouvions pas transporter cela.",
		"Nous ne pourrons pas transporter cela.",
		"Vous ne pouvez pas transporter cela.",
		"Vous ne pouviez pas transporter cela.",
		"Vous ne pouviez pas transporter cela.",
		"Vous ne pourrez pas transporter cela.",
		"Ils ne peuvent pas transporter cela.",
		"Ils ne pouvaient pas transporter cela.",
		"Ils ne pouvaient pas transporter cela.",
		"Ils ne pourront pas transporter cela."
	};

This is the can only take things test rule:
	try taking le Septentrion.

Chapter - Can't take what's fixed in place

RT-can't-take-what's-fixed-in-place-A is a response test.

For testing RT-can't-take-what's-fixed-in-place-A:
	now la table est dans la location; [Les supports sont fixés sur place par défaut.]
	test the can't take what's fixed in place rule response (A) with the can't take what's fixed in place test rule against {
		"C'est fixé sur place.",
		"C'était fixé sur place.",
		"C'était fixé sur place.",
		"Ce sera fixé sur place.",
		"C'est fixé sur place.",
		"C'était fixé sur place.",
		"C'était fixé sur place.",
		"Ce sera fixé sur place.",
		"C'est fixé sur place.",
		"C'était fixé sur place.",
		"C'était fixé sur place.",
		"Ce sera fixé sur place.",
		"C'est fixé sur place.",
		"C'était fixé sur place.",
		"C'était fixé sur place.",
		"Ce sera fixé sur place.",
		"C'est fixé sur place.",
		"C'était fixé sur place.",
		"C'était fixé sur place.",
		"Ce sera fixé sur place.",
		"C'est fixé sur place.",
		"C'était fixé sur place.",
		"C'était fixé sur place.",
		"Ce sera fixé sur place."
	};

This is the can't take what's fixed in place test rule:
	try taking la table.

Chapter - Use player's holdall to avoid exceeding carrying capacity

RT-use-player's-holdall-to-avoid-exceeding-carrying-capacity-A is a response test.

For testing RT-use-player's-holdall-to-avoid-exceeding-carrying-capacity-A:
	now la carrying capacity of le player est 2;
	now le player porte le sac;
	test the use player's holdall to avoid exceeding carrying capacity rule response (A) with line 1 of the use player's holdall to avoid exceeding carrying capacity test rule against {
		"(mettant le machin dans le sac pour faire de la place)",
		"(mettant le machin dans le sac pour faire de la place)",
		"(mettant le machin dans le sac pour faire de la place)",
		"(mettant le machin dans le sac pour faire de la place)",
		"(mettant le machin dans le sac pour faire de la place)",
		"(mettant le machin dans le sac pour faire de la place)",
		"(mettant le machin dans le sac pour faire de la place)",
		"(mettant le machin dans le sac pour faire de la place)",
		"(mettant le machin dans le sac pour faire de la place)",
		"(mettant le machin dans le sac pour faire de la place)",
		"(mettant le machin dans le sac pour faire de la place)",
		"(mettant le machin dans le sac pour faire de la place)",
		"(mettant le machin dans le sac pour faire de la place)",
		"(mettant le machin dans le sac pour faire de la place)",
		"(mettant le machin dans le sac pour faire de la place)",
		"(mettant le machin dans le sac pour faire de la place)",
		"(mettant le machin dans le sac pour faire de la place)",
		"(mettant le machin dans le sac pour faire de la place)",
		"(mettant le machin dans le sac pour faire de la place)",
		"(mettant le machin dans le sac pour faire de la place)",
		"(mettant le machin dans le sac pour faire de la place)",
		"(mettant le machin dans le sac pour faire de la place)",
		"(mettant le machin dans le sac pour faire de la place)",
		"(mettant le machin dans le sac pour faire de la place)"
	};
	now le carrying capacity of le player est 100;

This is the use player's holdall to avoid exceeding carrying capacity test rule:
	now le player porte le machin;
	now la babiole est dans la location;
	try taking la babiole.

Chapter - Can't exceed carrying capacity

RT-can't-exceed-carrying-capacity-A is a response test.

For testing RT-can't-exceed-carrying-capacity-A:
	now la carrying capacity of le player est 1;
	now le player porte le machin;
	now la babiole est dans la location;
	test the can't exceed carrying capacity rule response (A) with the can't exceed carrying capacity test rule against {
		"Je transporte déjà trop d'objets.",
		"Je transportais déjà trop d'objets.",
		"Je transportais déjà trop d'objets.",
		"Je transporterai déjà trop d'objets.",
		"Tu transportes déjà trop d'objets.",
		"Tu transportais déjà trop d'objets.",
		"Tu transportais déjà trop d'objets.",
		"Tu transporteras déjà trop d'objets.",
		"Il transporte déjà trop d'objets.",
		"Il transportait déjà trop d'objets.",
		"Il transportait déjà trop d'objets.",
		"Il transportera déjà trop d'objets.",
		"Nous transportons déjà trop d'objets.",
		"Nous transportions déjà trop d'objets.",
		"Nous transportions déjà trop d'objets.",
		"Nous transporterons déjà trop d'objets.",
		"Vous transportez déjà trop d'objets.",
		"Vous transportiez déjà trop d'objets.",
		"Vous transportiez déjà trop d'objets.",
		"Vous transporterez déjà trop d'objets.",
		"Ils transportent déjà trop d'objets.",
		"Ils transportaient déjà trop d'objets.",
		"Ils transportaient déjà trop d'objets.",
		"Ils transporteront déjà trop d'objets."
	};
	now le carrying capacity of le player est 100;

This is the can't exceed carrying capacity test rule:
	try taking la babiole.

Chapter - standard report taking A

RT-standard-report-taking-A is a response test.

For testing RT-standard-report-taking-A:
	test the standard report taking rule response (A) with the standard report taking A test rule against {
		"Voilà qui est fait.",
		"Voilà qui fut fait.",
		"Voilà qui a été fait.",
		"Voilà qui sera fait.",
		"Voilà qui est fait.",
		"Voilà qui fut fait.",
		"Voilà qui a été fait.",
		"Voilà qui sera fait.",
		"Voilà qui est fait.",
		"Voilà qui fut fait.",
		"Voilà qui a été fait.",
		"Voilà qui sera fait.",
		"Voilà qui est fait.",
		"Voilà qui fut fait.",
		"Voilà qui a été fait.",
		"Voilà qui sera fait.",
		"Voilà qui est fait.",
		"Voilà qui fut fait.",
		"Voilà qui a été fait.",
		"Voilà qui sera fait.",
		"Voilà qui est fait.",
		"Voilà qui fut fait.",
		"Voilà qui a été fait.",
		"Voilà qui sera fait."
	};

This is the standard report taking A test rule:
	now le machin est dans la location;
	try taking le machin.

Chapter - standard report taking B

RT-standard-report-taking-B is a response test.

For testing RT-standard-report-taking-B:
	now Gustave est dans la location;
	now le machin est dans la location;
	test the standard report taking rule response (B) with the standard report taking B test rule against {
		"Gustave ramasse le machin.",
		"Gustave ramassa le machin.",
		"Gustave a ramassé le machin.",
		"Gustave ramassera le machin.",
		"Gustave ramasse le machin.",
		"Gustave ramassa le machin.",
		"Gustave a ramassé le machin.",
		"Gustave ramassera le machin.",
		"Gustave ramasse le machin.",
		"Gustave ramassa le machin.",
		"Gustave a ramassé le machin.",
		"Gustave ramassera le machin.",
		"Gustave ramasse le machin.",
		"Gustave ramassa le machin.",
		"Gustave a ramassé le machin.",
		"Gustave ramassera le machin.",
		"Gustave ramasse le machin.",
		"Gustave ramassa le machin.",
		"Gustave a ramassé le machin.",
		"Gustave ramassera le machin.",
		"Gustave ramasse le machin.",
		"Gustave ramassa le machin.",
		"Gustave a ramassé le machin.",
		"Gustave ramassera le machin."
	};

This is the standard report taking B test rule:
	now le machin est dans la location;
	try Gustave taking le machin.

Part - Removing it from

Chapter - Can't remove what's not inside

RT-can't-remove-what's-not-inside-A is a response test.

For testing RT-can't-remove-what's-not-inside-A:
	now le machin est dans la location;
	now la table est dans la location;
	test the can't remove what's not inside rule response (A) with the can't remove what's not inside A test rule against {
		"Il n'est pas ici maintenant.",
		"Il n'était pas à cet endroit à ce moment.",
		"Il n'était pas à cet endroit à ce moment.",
		"Il ne sera pas à cet endroit à ce moment.",
		"Il n'est pas ici maintenant.",
		"Il n'était pas à cet endroit à ce moment.",
		"Il n'était pas à cet endroit à ce moment.",
		"Il ne sera pas à cet endroit à ce moment.",
		"Il n'est pas ici maintenant.",
		"Il n'était pas à cet endroit à ce moment.",
		"Il n'était pas à cet endroit à ce moment.",
		"Il ne sera pas à cet endroit à ce moment.",
		"Il n'est pas ici maintenant.",
		"Il n'était pas à cet endroit à ce moment.",
		"Il n'était pas à cet endroit à ce moment.",
		"Il ne sera pas à cet endroit à ce moment.",
		"Il n'est pas ici maintenant.",
		"Il n'était pas à cet endroit à ce moment.",
		"Il n'était pas à cet endroit à ce moment.",
		"Il ne sera pas à cet endroit à ce moment.",
		"Il n'est pas ici maintenant.",
		"Il n'était pas à cet endroit à ce moment.",
		"Il n'était pas à cet endroit à ce moment.",
		"Il ne sera pas à cet endroit à ce moment."
	};

This is the can't remove what's not inside A test rule:
	try removing le machin from la table.

Chapter - Can't remove from people

RT-can't-remove-from-people-A is a response test.

For testing RT-can't-remove-from-people-A:
	now Céleste est dans la location;
	now Céleste porte le machin;
	test the can't remove from people rule response (A) with the can't remove from people A test rule against {
		"Cela semble appartenir à Céleste.",
		"Cela semblait appartenir à Céleste.",
		"Cela semblait appartenir à Céleste.",
		"Cela semblera appartenir à Céleste.",
		"Cela semble appartenir à Céleste.",
		"Cela semblait appartenir à Céleste.",
		"Cela semblait appartenir à Céleste.",
		"Cela semblera appartenir à Céleste.",
		"Cela semble appartenir à Céleste.",
		"Cela semblait appartenir à Céleste.",
		"Cela semblait appartenir à Céleste.",
		"Cela semblera appartenir à Céleste.",
		"Cela semble appartenir à Céleste.",
		"Cela semblait appartenir à Céleste.",
		"Cela semblait appartenir à Céleste.",
		"Cela semblera appartenir à Céleste.",
		"Cela semble appartenir à Céleste.",
		"Cela semblait appartenir à Céleste.",
		"Cela semblait appartenir à Céleste.",
		"Cela semblera appartenir à Céleste.",
		"Cela semble appartenir à Céleste.",
		"Cela semblait appartenir à Céleste.",
		"Cela semblait appartenir à Céleste.",
		"Cela semblera appartenir à Céleste."
	};

This is the can't remove from people A test rule:
	try removing le machin from Céleste.

Book - Dropping

Chapter - Can't drop yourself

RT-can't-drop-yourself-A is a response test.

For testing RT-can't-drop-yourself-A:
	now Céleste est dans la location;
	now Céleste porte le machin;
	test the can't drop yourself rule response (A) with the can't drop yourself A test rule against {
		"Je manque de dextérité pour cela.",
		"Je manquais de dextérité pour cela.",
		"Je manquais de dextérité pour cela.",
		"Je manquerai de dextérité pour cela.",
		"Tu manques de dextérité pour cela.",
		"Tu manquais de dextérité pour cela.",
		"Tu manquais de dextérité pour cela.",
		"Tu manqueras de dextérité pour cela.",
		"Il manque de dextérité pour cela.",
		"Il manquait de dextérité pour cela.",
		"Il manquait de dextérité pour cela.",
		"Il manquera de dextérité pour cela.",
		"Nous manquons de dextérité pour cela.",
		"Nous manquions de dextérité pour cela.",
		"Nous manquions de dextérité pour cela.",
		"Nous manquerons de dextérité pour cela.",
		"Vous manquez de dextérité pour cela.",
		"Vous manquiez de dextérité pour cela.",
		"Vous manquiez de dextérité pour cela.",
		"Vous manquerez de dextérité pour cela.",
		"Ils manquent de dextérité pour cela.",
		"Ils manquaient de dextérité pour cela.",
		"Ils manquaient de dextérité pour cela.",
		"Ils manqueront de dextérité pour cela."
	};

This is the can't drop yourself A test rule:
	try dropping le player.

Chapter - Can't drop body parts

RT-can't-drop-body-parts-A is a response test.

For testing RT-can't-drop-body-parts-A:
	now le machin est une partie du player;
	test the can't drop body parts rule response (A) with the can't drop body parts A test rule against {
		"Je ne peux pas poser une partie de moi-même.",
		"Je ne pouvais pas poser une partie de moi-même.",
		"Je ne pouvais pas poser une partie de moi-même.",
		"Je ne pourrai pas poser une partie de moi-même.",
		"Tu ne peux pas poser une partie de toi-même.",
		"Tu ne pouvais pas poser une partie de toi-même.",
		"Tu ne pouvais pas poser une partie de toi-même.",
		"Tu ne pourras pas poser une partie de toi-même.",
		"Il ne peut pas poser une partie de lui-même.",
		"Il ne pouvait pas poser une partie de lui-même.",
		"Il ne pouvait pas poser une partie de lui-même.",
		"Il ne pourra pas poser une partie de lui-même.",
		"Nous ne pouvons pas poser une partie de nous-même.",
		"Nous ne pouvions pas poser une partie de nous-même.",
		"Nous ne pouvions pas poser une partie de nous-même.",
		"Nous ne pourrons pas poser une partie de nous-même.",
		"Vous ne pouvez pas poser une partie de vous-même.",
		"Vous ne pouviez pas poser une partie de vous-même.",
		"Vous ne pouviez pas poser une partie de vous-même.",
		"Vous ne pourrez pas poser une partie de vous-même.",
		"Ils ne peuvent pas poser une partie d'eux-mêmes.",
		"Ils ne pouvaient pas poser une partie d'eux-mêmes.",
		"Ils ne pouvaient pas poser une partie d'eux-mêmes.",
		"Ils ne pourront pas poser une partie d'eux-mêmes."
	};

This is the can't drop body parts A test rule:
	try dropping le machin.

Chapter - Can't drop what's already dropped

RT-can't-drop-what's-already-dropped-A is a response test.

For testing RT-can't-drop-what's-already-dropped-A:
	now le machin est dans la location;
	test the can't drop what's already dropped rule response (A) with the can't drop what's already dropped A test rule against {
		"Le machin est déjà ici.",
		"Le machin était déjà à cet endroit.",
		"Le machin était déjà à cet endroit.",
		"Le machin sera déjà à cet endroit.",
		"Le machin est déjà ici.",
		"Le machin était déjà à cet endroit.",
		"Le machin était déjà à cet endroit.",
		"Le machin sera déjà à cet endroit.",
		"Le machin est déjà ici.",
		"Le machin était déjà à cet endroit.",
		"Le machin était déjà à cet endroit.",
		"Le machin sera déjà à cet endroit.",
		"Le machin est déjà ici.",
		"Le machin était déjà à cet endroit.",
		"Le machin était déjà à cet endroit.",
		"Le machin sera déjà à cet endroit.",
		"Le machin est déjà ici.",
		"Le machin était déjà à cet endroit.",
		"Le machin était déjà à cet endroit.",
		"Le machin sera déjà à cet endroit.",
		"Le machin est déjà ici.",
		"Le machin était déjà à cet endroit.",
		"Le machin était déjà à cet endroit.",
		"Le machin sera déjà à cet endroit."
	};

This is the can't drop what's already dropped A test rule:
	try dropping le machin.

Chapter - Can't drop what's not held

RT-can't-drop-what's-not-held-A is a response test.

For testing RT-can't-drop-what's-not-held-A:
	now le machin est dans la cage;
	now le player porte la cage;
	test the can't drop what's not held rule response (A) with the can't drop what's not held A test rule against {
		"Je n'ai pas cela.",
		"Je n'avais pas cela.",
		"Je n'avais pas cela.",
		"Je n'aurai pas cela.",
		"Tu n'as pas cela.",
		"Tu n'avais pas cela.",
		"Tu n'avais pas cela.",
		"Tu n'auras pas cela.",
		"Il n'a pas cela.",
		"Il n'avait pas cela.",
		"Il n'avait pas cela.",
		"Il n'aura pas cela.",
		"Nous n'avons pas cela.",
		"Nous n'avions pas cela.",
		"Nous n'avions pas cela.",
		"Nous n'aurons pas cela.",
		"Vous n'avez pas cela.",
		"Vous n'aviez pas cela.",
		"Vous n'aviez pas cela.",
		"Vous n'aurez pas cela.",
		"Ils n'ont pas cela.",
		"Ils n'avaient pas cela.",
		"Ils n'avaient pas cela.",
		"Ils n'auront pas cela."
	};

This is the can't drop what's not held A test rule:
	try dropping le machin.

Chapter - Can't drop clothes being worn

RT-can't-drop-clothes-being-worn-A is a response test.

For testing RT-can't-drop-clothes-being-worn-A:
	test the can't drop clothes being worn rule response (A) with line 1 of the can't drop clothes being worn A test rule against {
		"(enlevant d'abord la veste)",
		"(enlevant d'abord la veste)",
		"(enlevant d'abord la veste)",
		"(enlevant d'abord la veste)",
		"(enlevant d'abord la veste)",
		"(enlevant d'abord la veste)",
		"(enlevant d'abord la veste)",
		"(enlevant d'abord la veste)",
		"(enlevant d'abord la veste)",
		"(enlevant d'abord la veste)",
		"(enlevant d'abord la veste)",
		"(enlevant d'abord la veste)",
		"(enlevant d'abord la veste)",
		"(enlevant d'abord la veste)",
		"(enlevant d'abord la veste)",
		"(enlevant d'abord la veste)",
		"(enlevant d'abord la veste)",
		"(enlevant d'abord la veste)",
		"(enlevant d'abord la veste)",
		"(enlevant d'abord la veste)",
		"(enlevant d'abord la veste)",
		"(enlevant d'abord la veste)",
		"(enlevant d'abord la veste)",
		"(enlevant d'abord la veste)"
	};

This is the can't drop clothes being worn A test rule:
	now le player arbore la veste;
	try dropping la veste.

Chapter - Can't drop if this exceeds carrying capacity

RT-can't-drop-if-this-exceeds-carrying-capacity is a response test.

For testing RT-can't-drop-if-this-exceeds-carrying-capacity:
	now le player porte le machin;
	[Avec un support.]
	now le tabouret est dans la location;
	now le player est sur le tabouret;
	test the can't drop if this exceeds carrying capacity rule response (A) with the can't drop if this exceeds carrying capacity test rule against {
		"Il n'y a plus de place sur le tabouret.",
		"Il n'y avait plus de place sur le tabouret.",
		"Il n'y avait plus de place sur le tabouret.",
		"Il n'y aura plus de place sur le tabouret.",
		"Il n'y a plus de place sur le tabouret.",
		"Il n'y avait plus de place sur le tabouret.",
		"Il n'y avait plus de place sur le tabouret.",
		"Il n'y aura plus de place sur le tabouret.",
		"Il n'y a plus de place sur le tabouret.",
		"Il n'y avait plus de place sur le tabouret.",
		"Il n'y avait plus de place sur le tabouret.",
		"Il n'y aura plus de place sur le tabouret.",
		"Il n'y a plus de place sur le tabouret.",
		"Il n'y avait plus de place sur le tabouret.",
		"Il n'y avait plus de place sur le tabouret.",
		"Il n'y aura plus de place sur le tabouret.",
		"Il n'y a plus de place sur le tabouret.",
		"Il n'y avait plus de place sur le tabouret.",
		"Il n'y avait plus de place sur le tabouret.",
		"Il n'y aura plus de place sur le tabouret.",
		"Il n'y a plus de place sur le tabouret.",
		"Il n'y avait plus de place sur le tabouret.",
		"Il n'y avait plus de place sur le tabouret.",
		"Il n'y aura plus de place sur le tabouret."
	};
	[Avec un contenant.]
	now la boîte est dans la location;
	now le player est dans la boîte;
	test the can't drop if this exceeds carrying capacity rule response (B) with the can't drop if this exceeds carrying capacity test rule against {
		"Il n'y a plus de place dans la boîte.",
		"Il n'y avait plus de place dans la boîte.",
		"Il n'y avait plus de place dans la boîte.",
		"Il n'y aura plus de place dans la boîte.",
		"Il n'y a plus de place dans la boîte.",
		"Il n'y avait plus de place dans la boîte.",
		"Il n'y avait plus de place dans la boîte.",
		"Il n'y aura plus de place dans la boîte.",
		"Il n'y a plus de place dans la boîte.",
		"Il n'y avait plus de place dans la boîte.",
		"Il n'y avait plus de place dans la boîte.",
		"Il n'y aura plus de place dans la boîte.",
		"Il n'y a plus de place dans la boîte.",
		"Il n'y avait plus de place dans la boîte.",
		"Il n'y avait plus de place dans la boîte.",
		"Il n'y aura plus de place dans la boîte.",
		"Il n'y a plus de place dans la boîte.",
		"Il n'y avait plus de place dans la boîte.",
		"Il n'y avait plus de place dans la boîte.",
		"Il n'y aura plus de place dans la boîte.",
		"Il n'y a plus de place dans la boîte.",
		"Il n'y avait plus de place dans la boîte.",
		"Il n'y avait plus de place dans la boîte.",
		"Il n'y aura plus de place dans la boîte."
	};

This is the can't drop if this exceeds carrying capacity test rule:
	try dropping le machin.

Chapter - Standard report dropping rule (joueur)

RT-standard-report-dropping-A is a response test.

For testing RT-standard-report-dropping-A:
	test the standard report dropping rule response (A) with the standard report dropping A test rule against {
		"Voilà qui est fait.",
		"Voilà qui fut fait.",
		"Voilà qui a été fait.",
		"Voilà qui sera fait.",
		"Voilà qui est fait.",
		"Voilà qui fut fait.",
		"Voilà qui a été fait.",
		"Voilà qui sera fait.",
		"Voilà qui est fait.",
		"Voilà qui fut fait.",
		"Voilà qui a été fait.",
		"Voilà qui sera fait.",
		"Voilà qui est fait.",
		"Voilà qui fut fait.",
		"Voilà qui a été fait.",
		"Voilà qui sera fait.",
		"Voilà qui est fait.",
		"Voilà qui fut fait.",
		"Voilà qui a été fait.",
		"Voilà qui sera fait.",
		"Voilà qui est fait.",
		"Voilà qui fut fait.",
		"Voilà qui a été fait.",
		"Voilà qui sera fait."
	};

This is the standard report dropping A test rule:
	now le player porte le machin;
	try dropping le machin.

Chapter - Standard report dropping rule (PNJ)

RT-standard-report-dropping-B is a response test.

For testing RT-standard-report-dropping-B:
	now Céleste est dans la location;
	test the standard report dropping rule response (B) with the standard report dropping B test rule against {
		"Céleste pose le machin.",
		"Céleste posa le machin.",
		"Céleste a posé le machin.",
		"Céleste posera le machin.",
		"Céleste pose le machin.",
		"Céleste posa le machin.",
		"Céleste a posé le machin.",
		"Céleste posera le machin.",
		"Céleste pose le machin.",
		"Céleste posa le machin.",
		"Céleste a posé le machin.",
		"Céleste posera le machin.",
		"Céleste pose le machin.",
		"Céleste posa le machin.",
		"Céleste a posé le machin.",
		"Céleste posera le machin.",
		"Céleste pose le machin.",
		"Céleste posa le machin.",
		"Céleste a posé le machin.",
		"Céleste posera le machin.",
		"Céleste pose le machin.",
		"Céleste posa le machin.",
		"Céleste a posé le machin.",
		"Céleste posera le machin."
	};

This is the standard report dropping B test rule:
	now Céleste porte le machin;
	try Céleste dropping le machin.

Book - Les pronoms dans les commandes

[Les pronoms pour les commandes sont définis dans l'array I6 LanguagePronouns avec un trait d'union initial, mais sont affichés sans ce trait d'union dans la réponse à la commande PRONOMS. Cet affichage est testé ici.]

[Les phrases pour pouvoir dire les pronoms depuis I7. La fonction I6 PronounWithoutHyphen est définie dans l'extension française.]

[Pour parcourir tous les pronoms. Adapté du repeat normal des Standard Rules.]
To repeat with (loopvar - nonexisting number variable) running through language pronouns begin -- end loop:
	(- for ({loopvar} = 1 : {loopvar} <= LanguagePronouns-->0: {loopvar} = {loopvar} + 3) -).

To say language pronoun (N - a number):
	(- PrintLanguagePronoun({N}, false); -)

To say language pronoun (N - a number) without hyphen:
	(- PrintLanguagePronoun({N}, true); -)

Include (-
[ PrintLanguagePronoun x without_hyphen;
	if (x < 1 || x > LanguagePronouns-->0) {
		print "ERROR: out-of-bound LanguagePronouns index";
	} else if (without_hyphen) {
		print (PronounWithoutHyphen) LanguagePronouns --> x;
	} else {
		print (address) LanguagePronouns --> x;
	}
];
-)

[La première assertion est là pour vérifier que tous les pronoms de LanguagePronouns sont présents dans la table of language pronouns et qu'ils sont donc bien testés.]
Simple unit test:
	repeat with X running through language pronouns:
		let pronoun tested be whether or not there is a pronom interne of "[language pronoun X]" in table of language pronouns;
		assert "language pronoun [language pronoun X] est testé" that pronoun tested equals true;
		if pronoun tested is true:
			choose row with pronom interne of "[language pronoun X]" in table of language pronouns;
			assert "affichage du language pronoun [language pronoun X]" that "[language pronoun X without hyphen]" exactly matches the text "[pronom affiché entry]";

Table of language pronouns
pronom interne	pronom affiché
"-le"	"le"
"-la"	"la"
"-l[']"	"l[']"
"-les"	"les"
"-lui"	"-lui"
"-leur"	"leur"
"-en"	"en"
"-y"	"y"
"lui"	"lui"
"elle"	"elle"
"eux"	"eux"
"elles"	"elles"

Reponses ends here.
